---
title: 'submodules'
---
'
[Home](../Home.md)

# adding a submodule

```
git submodule add <url to git repo>
```

e.g. :

- This is how I added the 'wiki' module to our main repo

```
git submodule add git@bitbucket.org:tmobile/tmo-one-site.git/wiki
```

- This is how I added the `config` module to our main repo

```
git submodule add git@bitbucket.org:tmobile/tmo-one-site-config.git config
```


# Initial Setup of this workspace

```
git clone git@bitbucket.org:tmobile/tmo-one-site.git
```

When you first clone the main repo you will have 2 folders inside that are placeholders for the submodules
```
config/
wiki/
```

You will need to init these *sub-modules* for access within your local workspace

```
git submodule init
```

you should also run the setup script to make sure you have all the tools you require

```
tools/setup/setup.sh
```

   or

```
tools/setup/setup.bat
```


# keeping the submodules up to date.

you will regularly need to sync your submodules
`git submodule update`

This will update the internal git repo for each submodule, which will also update the submodule sha1 pointer in your main repo config.
This pointer should always point to the current HEAD of the target branch in each repo.
When you commit changes to your main repo they will include the updates to the HEAD pointers for any defined submodules.

# Editing submodule contents

If you make any changes within a submodule you will need to commit those changes separately from within the submodule folder
i.e.
```
cd config
vi apps/shop/environments.yaml
git commit -am . "USxxxxx: added new deployment environment definition for shop app"
git push
```

# keeping main repo in sync with HEAD of submodules
