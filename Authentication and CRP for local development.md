
[Home](Home.md)

When you need test out an authenticated workflow, you can fake it as follows

## Add MyTMO cookies

Using Chrome dev tools, you can add these cookies to `localhost:3000`.

* `JSESSION`
* `IIII`
* `XSRF-TOKEN`
* `tmoUnav`

Any value would work for the first three. For `tmoUnav` it must be a JSON such as:

```
{"name":"Test User","switchAccount":"false","isRestricted":false}
```

![Screen Shot 2019-05-02 at 9.33.27 AM.png](https://bitbucket.org/repo/XX6G64E/images/3420442935-Screen%20Shot%202019-05-02%20at%209.33.27%20AM.png)

## Change your CRP_ID

The credentials endpoint points to the `localhost:3838` mock api server locally.

You can change the value it returns by editing `mock-data/api-mock-data/credentials.json`

The `CRP_ID` value will affect the pricing you see on PLP and PDP. By default it is `CRP6` when you are authenticated.

---

## Future Improvements

Some things that will make this better in the future.

* Remove need for mock data by pointing directly to a staging mytmo environment for authentication.
    * Will need local dev server to run on `.t-mobile.com` domain, so maybe we need to assign `localhost:3000` as `local.t-mobile.com` in our host files.
    * Need to have test accounts for developers to use on staging.

* Override the **Login** link in the unav header so that it points to the local mock api, sets the expected cookies automatically, and redirects user back to the original page.