---
title: 'Clear water sequence diagrams'
---
[Home](../../Home.md)

# Clearwater

This directory contains sequence diagrams for various components of the TOS applications.
<li> You should store uml diagrams under appropriate sub-directories. e.g. shop, pub, flex.
<li>Create the .puml file, and export/save it as .svg file.

