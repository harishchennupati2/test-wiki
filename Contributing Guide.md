[Home](Home.md)

This document outlines how a developer can contribute to the **tmo-one-site** repository.

## Submitting Pull Requests (what you should do)

When committing changes, make sure you are on a **feature branch**.

For example, if the [Rally](https://rally1.rallydev.com/) story has an ID of `US123456`, then a branch can be as follows.

```
#!bash
git checkout -b feature/US123456
```

Also make sure your **commit message** is prefixed with the same Rally ID.

```
#!bash
git commit -m 'US123456: Added foobar feature'

```

*Optionally*, you may want to **verify** your changes locally before submitting a PR.

```
#!bash
npm start affected.lint affected.test affected.build
```

This command will lint, test, and build only the apps and libs that are affected by the changeset. The affected functionality uses Nx to detect changes. For more information, visit the official [Nx documentation](https://nx.dev/).
 

Lastly, **push your branch** to origin.

```
#!bash
git push origin feature/US123456
```

And **open a pull request** in [BitBucket](https://bitbucket.org/tmobile/tmo-one-site/pull-requests/).

Next, let's see how the pull request gets merged into master.

---

### Continuous Integration (automation)

When a pull request is *opened*, the Jenkins pipeline will be started to verify your changes.

Jenkins will attempt to *automatically merge* your pull request if:

1. Verification succeeded (lint, test, build).
2. All reviewers have approved of the pull request.
3. There has not been new commits to the branch since Jenkins performed its verification.

The pull request will be updated with a comment detailing whether it has been merged or not, along with extra information if merge did not occur (e.g. what tests failed, etc.)