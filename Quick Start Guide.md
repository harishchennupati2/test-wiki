[Home](Home.md)

# Getting Started

Required items:

- Local administrative access to your workstation
- Windows 10 Pro **or** Mac OS **or** Linux

**Attention Windows Users!**

Please follow the setup guide here: [Windows Setup Guide](quick-start-guide-windows.md)

## Install Docker

Get Docker here: https://www.docker.com/products/docker-desktop

Docker is required to build, test and deploy applications within the `tmo-one-site` repo. Please make sure you have the latest version of `docker-CE` installed. This has the requirement that you are running OSX|Linux|Windows **10 Pro**.  
**DO NOT** use the deprecated _docker-tools_

## Install Git

If Git is not included on your system:

- You can download and install `git` here: [https://git-scm.com/downloads](https://git-scm.com/downloads)

### Mac (OSX 10+) users

- OSX users ( v10.00 + ) have `git` installed by default as part of the system.

  - In order to get the latest version of git you can use the download link above and follow the instructions,
    but the _**recommended**_ path is to use [HomeBrew](https://brew.sh/)

    - If you do not already have `HomeBrew` you can install using command line:

      ```bash
      $/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
      ```

    - or if you already have `HomeBrew` installed you can simply _update_ your local `brew casks` and _upgrade_ the installed packages.

      ```bash
      $ brew update && brew upgrade
      ```

    - Install `git` with `Homebrew`

      ```bash
      $ brew install git
      ```

    - Create Symbolic link to the version of `git` installed by `brew`.

      ```bash
      $ brew link --force git
      ```

#### Make sure you have the latest installation of `git` installed

```bash
$ git --version
git version 2.18.0
```

#### update git with brew

You can use the following commands to update your local installation of `git` (when installed by `brew`) to the latest.

```bash
$ brew update && brew upgrade
```

## Setup local workstation:

### Step 1 - Clone repository

Now that you have `git` you can clone the repository to your local workstation.

```bash
$ git clone --recurse-submodule git@bitbucket.org:tmobile/tmo-one-site.git
$ cd tmo-one-site
```

### Step 2 - Initialize & Update submodules

```bash
$ git submodule init
$ git submodule update
$ cd config
$ npm install
```

### Step 3 - set local commit user account

using the same email you used to create your bitbucket.org account:

```bash
$ git config user.email "${my-email@t-mobile.com}"
$ git config user.name "${my-full-name}"
```

### Step 4 - Run webdev-setup-tools

Navigate to your local copy of the repo and into tools/setup. There are a pair of scripts here which can set up your system. If you have a Windows system, run the `setup.bat` file from Powershell. If you have a Mac or a Linux system, run the `setup.sh` file.

What is the difference?

These scripts inspect your system and make sure you have the latest _compatible_ version of `nodejs` installed.

- For `Linux` and`OSX` systems it will install `node` using `nvm` (_node version manager_).
- For `Windows` users it will validate that you have the correct permissions before installing the latest _compatible_ `node.exe`.

If you already have the latest compatible version of node installed these scripts will do nothing but pass control over to `setup.js`

Once you have the correct version of `node` installed the `setup` scripts you pass control over to `node setup.js` for the remainder of the process.

`setup.js` will make sure you have the correct node global packages installed and the AEM dependencies.

---

### Step 5 - Install local dependencies

Make sure you are on the T-Mobile VPN, and

```bash
$ npm install
```

## Optional - Install AEM

If you are going to be interacting with Adobe Experience Manager (AEM), consult the [AEM Quick Start Guide](AEM%20Quick%20Start%20Guide.md).

---

Next, read through the [Developer Guide](Developer%20Guide.md) for how to perform tasks in the project.
