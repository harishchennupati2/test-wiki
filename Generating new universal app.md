[Home](Home.md)

# Generating new client-server application

To create a new universal app (e.g. with server-side rendering), run:

```
npm start "app-gen [name]"
```

Where `[name]` is the name you want to give the app. For example, `hello-world`.

This will generate the client project (Angular), server project (Hapi), and the SSR setup (Angular Universal), as well as update configuration files (`package-scripts.js`, `nx.json`, `angular.json`, etc.).

After the files are generated, you can start the server with:

```
npm start [name].dev
```

For more information, check out the [Developer Guide](Developer%20Guide.md).