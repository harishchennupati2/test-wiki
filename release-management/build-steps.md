---
title: 'Build-steps'
---
[Home](../Home.md)

# Build Steps

return to main [readme](../README.md)

### this is what happens under the hood -- it is a sequence of aliases:
1. `npm start` -->
   1. `npm run start:dev` -->
      1. `npm run start:shop` -->
         1. `concurrently "npm run server:shop-client:watch" "npm run start:shop:client" "npm run start:shop-server"` -->
            1. `npm run server:shop-client:watch` -->
               1. `ng run shop-client:server --watch`
            2. `npm run start:shop:client` -->
               1. `ng serve shop-client --open`
            3. `npm run start:shop-server` -->
               1. `npm run hydrate && ng serve shop-server` -->
                  1. `npm run --prefix ./config/ hydrate && node tools/postBuild`
                     1. **execute the following inside `./config/`**
                        1. `npm run validateyamlnode ./libs/hydrate.js` -->
                           1. `pajv validate -s ./apps/yamlconfigschema.json -d \"./apps/**/!(environments).y?(a)ml\"`
                           2. `pajv validate -s ./apps/yamlenvironmentschema.json -d \"./apps/**/environments.y?(a)ml\"`
                        2. `node ./libs/hydrate.js`
                     2. `node tools/postBuild`
                  2.  `ng serve shop-server`


thus, the single command

`npm start`

results in the following actions being run concurrently:

- `ng run shop-client:server --watch`
- `ng serve shop-client --open`
  - start the **shop** backend server (Hapijs with webpack http server with HMR (Hot Module Reloading).
    - Webpack HMR compiles all client code into memory and allows any new changes to be compiled and replace the code in memory without restarting the server.
  - open default browser to **shop** url [http://localhost:4200](http://localhost:4200)
- `npm run --prefix ./config/ hydrate && node tools/postBuild`
  - hydrate all config settings inside `./config/` submodule
  - copy hydrated config files to `tmo-one-site/dist` folder
- `ng serve shop-server`
