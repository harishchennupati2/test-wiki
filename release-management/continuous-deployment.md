---
title: 'Continous-deployment'
---
[Home](../Home.md)

## Setting up Continuous Deployment

**Getting the list of affected apps**

Apps that are deployment targets must have corresponding "build:[name]" tag specified in `nx.json`.

For example, both `shop-client` and `shop-server` apps have `build:shop` as a tag.

In the CD pipeline, the node script `tools/cd/list-affected` will write the affected apps out to `affected_apps.txt`.