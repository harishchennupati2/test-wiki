[Home](Home.md)

# Generating new API (Hapi plugin and Angular service)

This document outlines how you can create a new Hapi plugin and a corresponding Angular service.

The example is shown for generating "hello" endpoint that takes a name, and returns a message as "Hello [name]".

## 1. Generate with API schematic

The `api-gen` script will generate the new endpoint using a workspace schematic (`tools/schematics/api`).

For example,

```
npm start "api-gen --name hello"
```

## 2. Generate plugin and service files

Run the `openapi-codegen` script to generate the Hapi plugin (server) and Angular service (client).

```
npm start openapi-codegen
```

Note: This will take a while to run on the first run. Subsequent runs will be much faster.

## 3. Add plugin to Hapi server

Edit your server main and register the new plugin.

For example, for shop it is `apps/shop/server/src/server.ts`.

**server.ts**

```
// ...
import { helloPluginFactory } from '@tmo/hapi/hello';

// ...

const hapiConfig: TMOHapiConfig = {
  appName: 'shop',
  appVersion: '0.0.1',
  plugins: [
    // ...
    { hapiPlugin: helloPluginFactory() }
  ]
};

// ...
```

## 4. Injecting to Angular application

Edit your `app.module.ts` file. (e.g. `apps/shop/client/src/app/app.module.ts`) and add an import for the Angular service module.

**app.module.ts**

```
// ...
import { ApiModule as HelloApiModule } from '@tmo/shared/hello-api-angular';

// ...

/* app */
@NgModule({
  declarations: [AppComponent],
  exports: [AppComponent],
  imports: [
    // ...
    HelloApiModule
  ],
  // ...
})
// ...
```

Now you can inject `HelloApiService` from `@tmo/shared/hello-api-angular` into any component or service that needs it.

For example,

**hello.component.ts**

```
import { HelloApiService } from '@tmo/shared/hello-api-angular';
// ...

@Component({
 // ...
})
export class AppComponent implements OnInit {
  constructor(private readonly service: HelloApiService) {}

  ngOnInit() {
    this.service.getHello({})
      .subscribe(resp => console.log(resp.message));
  }
}
```

---

## Additional Info

### Authenticating additional requests

If you need to call additional endpoints, you can generate an oauth token and provide it in the `Authorization` header.

**hello-plugin.ts**

```
// ...
import { getOauthToken } from '@tmo/hapi/domain/dcp';
import { HapiServerPlugin, RestClient } from '@tmo/hapi/utils';

export const helloPluginFactory: () => HapiServerPlugin = () =>
  new HelloApiPlugin({
    getHello: async ({ request }) => {
      const oauthToken = await getOauthToken();

      // Call out to some endpoint with oauth token
      const resp: any = await RestClient.invokeApi({
        method: 'POST',
        baseURL: 'http://api.hello.com',
        url: '/ping',
        headers: {
          'Content-Type': 'application/json',

          // Pass the token
          authorization: `Bearer ${oauthToken.access_token}`,

          // ...
        }
      });

      return { message: `Hello ${request.name}` }
    }
  });
```

**Note:** You may need to provide additional headers. Check with the maintainer of the API.