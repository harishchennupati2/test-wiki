---
title: 'Web Security archive'
---
# Web Security Archive

Some research notes on web security across distributed client architectures.

These are thoughts that are not complete or fully vetted out but I wanted to keep them around to remind myself of directions we were investigating.
