---
title: 'pop-jwt'
---
## Handoff
during an client handoff the source client injects the
pop tokens ( a_token, id_token ) into the Secure HTTP Only cookies.
then adds a nonce token for the request into the request headers

if request is served from a CDN cache this nonce will be lost
so perhaps in handoff requests we need to move the nonce to the session cookies.

client can check cookies for nonce.  If nonce exists the request is a handoff.
client will move nonce from session cookie to Secure HTTP Only cookie.
at this point the secure cookies will contain 1 or both pop tokens and the nonce.


## fresh client handoff:

** no pre-existing a_token in sessionStorage. **

client then adds client 'aud' and 'cnf' to custom headers along with a new 'nonce' token for this request
and makes a PUT request to /auth.  This is a request to update the secure cookies with new tokens
generated using the new client pop token values (aud, cnf).
this request cannot be popped as the pop tokens do not exist yet.
hapi will make requests for replace the pop tokens found in http only cookies using the new 'aud' and 'cnf'


## Question:
if MITM captures a request, with included handoff pop-tokens
in hidden server-side domain specific cookies,
what stops them from making a PUT: /auth using their own 'cnf' ?



## NOTE!  ########
The client *needs* to see the 'id_token' contents; this will include
an account id and an array of phone lines assigned to account

So, should only the a_token be considered a 'pop' and hide in the session only cookies
while the 'id_token' is visible in the session cookies ?

You can *only* be logged into 1 account, regardless of the number of applications (a_tokens) you pass thru
Each application you pass thru must generate a new 'a_token' for that application
and use that as the 'pop' for the 'nonce' of every request.

Back to the ## Question ##
what stops MITM from issuing a
PUT: https://www.t-mobile.com/api/auth
passing their own 'cnf'



If handoff source pushes pop tokens into http only cookies
and includes the handoff uri in the nonce signature

when receiving (target) client makes POST: /api/auth to get new a_token and includes a custom header for original request uri
then hapi can validate the nonce hidden in the http only cookies (if it exists), generate a new a_token using the 'cnf' and 'aud' passed in the custom headers and the 'usn' found in 'a_token' (http only cookie).
Then hapi can swap the 'id_token' using the same 'usn' and 'cnf'.
Finally hapi can add all the pop tokens into the http only cookie jar.

client needs to see the 'id_token' but does not need to see the 'a_token'


if 'id_token' is visible to 'client' and 'a_token' is not, and hapi *requires* both to be available on all requests, and that the 'cnf' and 'aud' on each *must* match.

Then on the handoff the first thing that client does is request an a_token then follow with an /api/auth/validate to validate the original request

## nonce timeout

put a short expiry on the nonce (<5 seconds) to limit amount of time for MITM to calculate new request.



When target client is loaded it will check sessionStorage for an anonymous token,
if found it will make an /auth/validate claim using this token
If not found it will make a GET /auth/token passing the 'cnf' and 'aud'



## what if

we do not swap out tokens, merely append them:

```
{
  "iss": "https://server.example.com",
  "aud": "https://client.example.org",
  "exp": 1361398824,
  "iat": 1234234123,
  "cnf": [
     {
        "aud": "TMOApp",
        "kid":"dfd1aa97-6d8d-4575-a0fe-34b96de2bfad"
     },
     {
        "aud": "TMNG",
        "kid":"efad25bb-6d8d-4575-a0fe-29cd54fa3ac"
     }
   ]
}
```

And the "aud" *must* be part of a known list of clients
And the nonce contained a 3rd claim: "aud" to identify the
client key used to signed the nonce.




Cookies.gi pull
https://blog.webf.zone/ultimate-guide-to-http-cookies-2aa3e083dbae



------------------------------------------------------

## RFC 7800 : Proof Of Possession Tokens
## https://tools.ietf.org/html/rfc7800

the presenter generates a public/private key pair and
(1) sends the public key to the issuer,
 which creates a JWT that contains the public key in the confirmation claim.
The entire JWT is integrity protected using a digital signature to protect it against
 modifications.
The JWT is then (2) sent to the presenter.

When the presenter (3) presents the JWT to the recipient, it also needs to
demonstrate possession of the private key.  The presenter, for example,
-  (4) uses the private key in a Transport Layer Security (TLS) exchange with the recipient
 or
-  (4) signs a nonce with the private key.

The recipient is able to verify that it is interacting with the
 genuine presenter by extracting the public key from the confirmation
 claim of the JWT (after verifying the digital signature of the JWT)
 and utilizing it with the private key in the TLS exchange
 or
 by checking the nonce signature.
