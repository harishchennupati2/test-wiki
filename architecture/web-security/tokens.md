---
title: 'tokens'
---

[Home](../Home.md)


# encryption

| key | description |
|---|---|
|pgp| Pretty Good Privacy|
|gpg| Gnu Privacy Gaurd|
|sha| secure hashing algorithm|
|uuid| Universally Unique IDentifier|
|guid| Globally Unique IDentifier|


# Tokens

| key | description |
|---|---|
|uuid| Universally Unique IDentifier|
|guid| Globally Unique IDentifier|
|jwt| JSON Web Token|
|jws| JSON Web Signature|


# Jwt

| key | description |
|---|---|
|dat| Device Access Token|
|aat| Anonymous Access Token|
|idt| IDentity Token|
|pop| Proof of Possession|


### dat

```
{
    cnf: pgp-pub-key,
    usn: client-uuid,
    ... { device identification data fields - 'anonymous' identification of specific device }
} : signed by brass private-key
```

### idt
```
{
    sub: user-id,
    aud: client-id,
    cnf: pgp-pub-key,
    usn: client-uuid
    ... { user authentication fields {
} : signed by brass private-key
```

| key | Name |description |
|---|---|---|
|sub| Subject| user id|
|aud| Audience| client application id|
|cnf| ?? | browser pgp public key|
|usn| Unique Session Number| Assigned by IAM|




### pop
```
{
    ehts: [ 'dat','idt','body','url','method'],
    edts: 256sha hash of concatenated request values identified by keys in `eiht`
} : signed by client pgp-private-key
```


- `ehts`:  _*Enterprise Header Token Security*_
- `edts`:  _*Enterprise Data Token Security*_


---

## Examples of JSON Web Tokens (jwt)

### dat

This is a **Device Access Token** ( `dat` ) as defined by the native app (ios/android). The following value is uuencrypted.

>**eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlVGQkVSRUZVTFRJd01Uaz0ifQ**.eyJpc3MiOiJodHRwczovL3BwZC5icmFzcy5hY2NvdW50LnQtbW9iaWxlLmNvbSIsImF1ZCI6IlVOS05PV04iLCJuZXR3b3JrIjp7Im1zaXNkbiI6IjQyNTQzNTEyNDkiLCJpbXNpIjoiMzEwMjYwMTUzOTk5Mzg2IiwiaW1laSI6IjM1NTgzMjA4Mjc3NjY5MjciLCJnZ3NuaXAiOiIxNzIuNTcuNjcuMTYwIiwic2dzbmlwIjoiMTcyLjU3LjY3LjE2MyJ9LCJkZXZpY2UiOnsiY25mIjoiLS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS1cbk1JSUJJakFOQmdrcWhraUc5dzBCQVFFRkFBT0NBUThBTUlJQkNnS0NBUUVBcVBaMk1pS243UElETXJtS0puYlRcbmpWaVZ3akhZV3padDFZZU9kczUyNTRoNVNnbjdqV3hJRjFxVTdGc0hRMG0xTHBaR0lDYlVtbWgxc0duWWtkQ3ZcbnVqVUNLVDZVTHJuelFHVm1helpLS1BpS0FSdlBJQytSSlJoNlBuTmtjM1RMVTdWakF0T29GZ3RSZU5pZlJHRCtcbjVlOGZpb2EvT2hPNVZaVVYzejZSWDBTSWhWNWI0MlBOU0pYYjFGeWMvQ09CaXFDaWZLMDIvREJrdjlISU4yb1RcbnFJSkFwVkhGWnYxSDVJTTBpb2U5emoraVBZcFRjL1BTZnNhTm9kUTNROVRSZ2w2MFB3U2tEV3ZPOExHbTk5QnJcbjAweWJma1gyUmRQTkY3dXpMdnRuYXpTYU9aT1JCelpMbG9BN3hoWWY0NVFCMWgyT2trelVGS0syUnJEaGNwSVFcbjB3SURBUUFCXG4tLS0tLUVORCBQVUJMSUMgS0VZLS0tLS1cbiJ9LCJleHAiOjE1NTU0NDc5MzQsImlhdCI6MTU1NTM2MTUzM30.**wp_W5b2rJv7oCIeWYo0AThAtgrAvOKip67eS-HsLuNb2OsXaUoycZQd-8n5fY2nIxHo2MjRXmYsiZPOicH5p_6n-HIZGMB_AVHkAgD_rW-ETXGuswO7dSfbMlHfgQ9Fif1fR_dqG_akgg2QP2EwVlOXg_Lwud2j_pkgAAoXdnqcS_UQHWJFdBTRzZ1nN-E3hk6f1PSQGpF1KeWqbNLpMxk9WAtBPxQ6VByp7qaxoYjvIgscGfMZYMJ8R8WekEO9ArZObx8L8V6dArBWJeBjdaeLprd0WfLhWPYkicOte364xvss39tmXE0rvVtEOGrqVgcEUvWeyN66Jt69Hpbj02A**


Copy and past this token into the following web site

[https://jwt.io/](https://jwt.io/)

and you will see the following deconstruction of the decoded token

---

#### header

```
  {
    "alg": "RS256",
    "typ": "JWT",
    "kid": "UFBEREFULTIwMTk="
  }
```


| key | Name |description |
|---|---|---|
|alg|Algorithm| Which hashing algorithm is used to sign this token|
|typ|Type| What kind of token is this (_JWT_)|
|kid|||

The header defines that a RS256 hash algorithm was used to sign this token.

---
#### payload


```
  {
    "iss": "https://ppd.brass.account.t-mobile.com",
    "aud": "UNKNOWN",
    "network": {
      "msisdn": "4254351249",
      "imsi": "310260153999386",
      "imei": "3558320827766927",
      "ggsnip": "172.57.67.160",
      "sgsnip": "172.57.67.163"
    },
    "device": {
      "cnf": "-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqPZ2MiKn7PIDMrmKJnbT\njViVwjHYWzZt1YeOds5254h5Sgn7jWxIF1qU7FsHQ0m1LpZGICbUmmh1sGnYkdCv\nujUCKT6ULrnzQGVmazZKKPiKARvPIC+RJRh6PnNkc3TLU7VjAtOoFgtReNifRGD+\n5e8fioa/OhO5VZUV3z6RX0SIhV5b42PNSJXb1Fyc/COBiqCifK02/DBkv9HIN2oT\nqIJApVHFZv1H5IM0ioe9zj+iPYpTc/PSfsaNodQ3Q9TRgl60PwSkDWvO8LGm99Br\n00ybfkX2RdPNF7uzLvtnazSaOZORBzZLloA7xhYf45QB1h2OkkzUFKK2RrDhcpIQ\n0wIDAQAB\n-----END PUBLIC KEY-----\n"
    },
    "exp": 1555447934,
    "iat": 1555361533
  }
```

#### -- definition --


    iss:  Issuing Authority,
    aud:  Audience,
    network: {
      msisdn:  Mobile Station International Subscriber Directory Number,
      imsi:    International Mobile Subscriber Identity,
      imei:    International Mobile Equipment Identity,
      ggsnip:  172.57.67.160,
      sgsnip:  172.57.67.163
    },
    device: {
      cnf: *browser*|device pgp public key

    },
    exp: timestamp when token will expire,
    iat: timestamp when token was issued (issuer activate time)



The payload of this token contains information to discretely identify the specific physical device (mobile phone) that is making the request.
The `imei` and `imsi` values are specific to the chip in that phone.
the `msisdn` is the phone number currently associated with that device.

---

#### signature
`wp_W5b2rJv7oCIeWYo0AThAtgrAvOKip67eS-HsLuNb2OsXaUoycZQd-8n5fY2nIxHo2MjRXmYsiZPOicH5p_6n-HIZGMB_AVHkAgD_rW-ETXGuswO7dSfbMlHfgQ9Fif1fR_dqG_akgg2QP2EwVlOXg_Lwud2j_pkgAAoXdnqcS_UQHWJFdBTRzZ1nN-E3hk6f1PSQGpF1KeWqbNLpMxk9WAtBPxQ6VByp7qaxoYjvIgscGfMZYMJ8R8WekEO9ArZObx8L8V6dArBWJeBjdaeLprd0WfLhWPYkicOte364xvss39tmXE0rvVtEOGrqVgcEUvWeyN66Jt69Hpbj02A`

---


### idt

#### Example of a Signed IDentity Token

eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlVGQkVSRUZVTFRJd01Uaz0ifQ.**eyJpYXQiOjE1NTQ5MjcxNzEsImV4cCI6MTU1NDkzMDc3MCwiaXNzIjoiaHR0cHM6Ly9wcGQuYnJhc3MuYWNjb3VudC50LW1vYmlsZS5jb20iLCJhdWQiOiJUTU9BcHAiLCJBVCI6IjAxLlVTUi5yRDRPbmd2UXZxbmVLV2FNVSIsInN1YiI6IlUtZWMxYzBlMmQtNzQ1NS00OGJiLTgwOTYtOGJiNDExMmQ1M2Q3IiwiYWNyIjoibG9hMiIsImFtciI6WyJwYXNzd29yZCIsInNlY3VyaXR5X3F1ZXN0aW9uIl0sInVzbiI6IjgwODNiOTA4M2VkMzIxMGMiLCJlbnRfdHlwZSI6ImRpcmVjdCIsImVudCI6eyJhY2N0IjpbeyJyIjoiQU8iLCJpZCI6Ijk2NTgwNzg5NyIsInRzdCI6IklSIiwibGluZV9jb3VudCI6MiwibGluZXMiOlt7InBobnVtIjoiNDI1NDM1MTI0OSIsInIiOiJEIn1dfSx7InIiOiJBTyIsImlkIjoiOTY2MDUyMzYzIiwidHN0IjoiSVIiLCJsaW5lX2NvdW50IjoyLCJsaW5lcyI6W3sicGhudW0iOiI0MjUzODk5MTUyIn1dfV19fQ**.ogJ_wf_tW0iFX6GkFL_Fw7_VQD_qaqTxN3jSyPT7NOAYN6MWbvbEOApGFHT-o7sLMtZVoIsTCzr0AMrCLMUwchIPJSTwoqlWfUAuevguTOjhDbWnJhOhG8sO0faqAMYlZy305wBNvcV8OWvHWOVX-umBhBbBpThDXGX-8Vl9Vb_4cGsaHLbo8KULzWRKfNZsEhjr9tE3SwttUAnJS8eQFSGGfpWUOsH35mtx2v5v33WGAWowYqjgGv49gEwm9yStsS8p3yvCKlWPEslBvR5yoAsJCSJ1JCvXU1f9zaDyD_HRaTz30eU0cDABqqLvwhFTE95jjJlHnC-wQRFTA4v1Zw


#### header:
```
{
  "alg": "RS256",
  "typ": "JWT",
  "kid": "UFBEREFULTIwMTk="
}
```

#### payload
```
{
  "iat": 1554927171,
  "exp": 1554930770,
  "iss": "https://ppd.brass.account.t-mobile.com",
  "aud": "TMOApp",
  "AT": "01.USR.rD4OngvQvqneKWaMU",
  "sub": "U-ec1c0e2d-7455-48bb-8096-8bb4112d53d7",
  "acr": "loa2",
  "amr": [
    "password",
    "security_question"
  ],
  "usn": "8083b9083ed3210c",
  "ent_type": "direct",
  "ent": {
    "acct": [
      {
        "r": "AO",
        "id": "965807897",
        "tst": "IR",
        "line_count": 2,
        "lines": [
          {
            "phnum": "4254351249",
            "r": "D"
          }
        ]
      },
      {
        "r": "AO",
        "id": "966052363",
        "tst": "IR",
        "line_count": 2,
        "lines": [
          {
            "phnum": "4253899152"
          }
        ]
      }
    ]
  }
}
```


| key | Name |description |
|---|---|---|
|iat|||
|exp|||
|iss|||
|aud|||
|AT|||
|sub|||
|acr|||
|amr|||
|-|password||
|-|security_question||
|usn|||
|en_type|||
|ent|||
|-|acct|||
|-|r|||


