---
title: 'Scenarios'
---
# User Scenarios for Web Authentication

### POP Signatures

(Proof of key possession is commonly abbreviated as POP.)

https://community.apigee.com/articles/71494/pop-using-jwt-to-prove-possession-of-a-key.html


For TOS applications we have a number of scenarios for POP-Signature creation and validation.


## 1. Cold Start

New browser session with no domain cookies for pop-signature tokens.


## 2  Warm Start

Browser session was already warmed (prior visit to a TOS app), existence of one or both poppable tokens ( `bst` , `idt` ) in session cookies. The minimum is the `bst`, all TOS applications must generate a `bst` and store in session tokens.


## 3. Handoff (redirect)

Another TOS application has redirected to this TOS app.  The app handoff will include a complete pop-signature.

a special form of the *Handoff* scenario is from the native app (iOS | Android ) to a web view.

>The 'App' opens a WkWebView browser widget and passes in a page request using custom headers instead of tokens.
>This is due to the nature of the limitations that the WkWebView widget has with generating cookies.


### Native App

If the request is coming from a native app (iOS / Android) the pop-signature will be found in custom headers.
The CDN/HAProxy layer will see these custom headers and convert them to *_`server only https session cookies`_*.
This means that the cookies are only visible on the server side, not by client side javascript.

#### CDN Response: cached pre-rendered view

The browser will receive a pre-rendered page response from the CDN cache.
The handoff `pop-signature` will be in `server only https cookies`.


After the DOM is loaded and parsed it will request the service-worker script.
Since there is no chance of a prior session in this particular handoff the client script will follow the web-app fresh start process.

When the `/api/auth` request is received at the web-server it will notice that the cookies contain the pop-signature from another application.  First the request handler will validate the pop-signature.

If the pop-signature is valid the request handler will make a request to `swap` the `bst` ( and the optional `idt` if it exists ) from the BRASS Token Management API


##### BRASS Token Management API

###### `TMS`: _authenticated_ Token Management System

**swagger**

>https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/secureid/brass-tms-v3-apigee-updated.json?at=US634412


_ eg: swap identity token (`idt`) _

**PUT**: `https://dev.brass.account.t-mobile.com/tms/v3/usertoken`
```
{
   'aud': <new client id>,
   'cnf': <new client public key>,
   'token': <'idt' from request cookies>
}
```





###### `ATMS`: _anonymous_ Token Managment System

**swagger**

>https://bitbucket.service.edp.t-mobile.com/projects/EITCODEDOC/repos/flow-documentation/browse/swagger/secureid/brass-tms-anonymous-v3-apigee.json?at=refs%2Fheads%2FUS634411-apigee-proxy-for-tms-anonymous


_eg: swap browser session token `bst`|`dat`_

**PUT**: _https://dev.brass.account.t-mobile.com/ats/v1/anonymous/usertoken_
```
{
   'aud': <new client id>,
   'cnf': <new client public key>,
   'token': <'dat' from request cookies>
}
```

the `/api/auth` request will respond with 2 new tokens in session cookies that *can* be read by the client javascript.  These tokens will contain the `aud` *and* `cnf` of the source browser as well as the *usn* of the handoff initiating application.  This will allow continuity of both the browser session and the authenticated session.



When the page response is loaded into the browser it will

## Web App

# 1. Cold Start

A new browser instance (or new tab) has navigated to an application url, this could be froma bookmark, link or manual entry.  Since this is a fresh start there will not be any pre-exising tokens in the cookies or sessionStorage.

After the DOM Content is loaded and parsed in the browser a request will be made for the service-worker. The service worker should load, parse and initialize *before* the main application is loaded.  The service-worker will look for an existing crypto-key pair in the domain specific IndexDB within the browser.  If this key-pair does not exist the worker will generate a new pair and store in the IndexDB for the current app domain < tmobile.com | my.tmobile.com >.

Next the worker will look in the cookies for an existing pop-signature. Since this is a fresh start there will not be one.

Finally the worker will request a new browser session token (`bst`) using the web-servers `/api/auth` request.

**POST**: _https://{app domain}/api/auth_
```
{
   "aud": <current client-id>
   "cnf": <current client public key (as stored in indexDb)>
}
```


The web server request handler for `/api/auth` will first look for an existing pop-signture. Since this is a *Fresh Start* there will not be any existing pop-signature tokens.

Next the `/api/auth` handler will request a new anonmyous browser session token from *ATMS*.

#### eg: create new browser session token `bst`|`dat`

**POST**: _https://dev.brass.account.t-mobile.com/ats/v1/anonymous/usertoken_
```
{
   'aud': <new client id>,
   'cnf': <new client public key>,
}
```

This will return a new `bst` with the following fields:

```
{
   "sub": "anonymous",
   "aud": <client-id>,
   "cnf": <client public-key>,
   "usn": <UUID generated by ATMS>
   "expiry": <timestamp configured to expire in 24 hours>
   ...
}
```

The expiry of the `bst` is set very long becuase it is used to identify the browser session, which _could_ be open for many hours, even days (or in my case weeks).  24 hrs seems reasonable, this would force the token to be refreshed with a new session at least once every 24 hours.

This new `bst` is added to the response session cookies and a 200 response is returned.

If this token is captured by a 3rd party it is of no use to them as they will not have the corresponding private key to sign requests with.

### pre-rendered view

Even though we are using server-side rendering to deliver pre-rendered views to the browser, the authentication request to generate or swap pop-signature tokens must be performed by the browser as that is the only place that the private-key is stored.


## CDN:

requested page has already been pre-rendered and stored in the CDN. The request will be resolved from the CDN.


## SSR:

Cache miss at CDN.  Request is routed to app server for pre-rendering.  During SSR pre-rendering the SSR handler will need to establish an anonymous token before it can pre-render  the view.



# 2. Warm Start

A pre-existing browser tab that previously visited a OneSite app.
This tab (browser session) will have one or more anonymous tokens stored in the session Storage.

If a token is stored with the appropriate application client id ( "aud": <audience> ) then that token can be used to continue the browser session for anonymous access.

If an authenticated token (idt) is found for the app client that has not expired then it can be used for continued access.




----

# Notes:


### Session Storage

This is where session constrained data is stored within the browser.  All data is *only* accessible by applications from the same domain (no XSS), hence the following two sub-domains

www.t-mobile.com
www.my.t-mobile.com

will have 2 different (exclusive) session storage buckets.

What if user is logged in and they deep link to another application ?
i.e.

1. navigate to authenticated site  `www.my-t-mobile.com/`
2. login and view the authenticated home page
   - at this point your browser has a `wat` and `idt` in sessionStorage
3. click browser bookmark for shopping page   `www.t-mobile.com/cell-phones/apple`.







## App Cookies

A TOS application will use a variety of cookies to help track the customer. across calls and between browser restarts.

Cookies are part of the HTTP header, so they can not be safer than themselves. Cookies have security flags built into their specification: HTTPOnly and Secure, the latter of which prevents transmission over non-SSL connections.


If you are passing authorisation token via http headers then you need to have a client side logic to pass this to server every time you make a request. A skimmer can look for this in your client side code and can hijack your user session with Java script.

But if the same info is passed via cookies then it is the browsers responsibility to pass the cookie whenever a request is made(you are freed of writing client side logic). So making it a bit difficult(but not impossible) to identify the mechanism in which the token is being passed.

If the cookie is set to be *httponly* then session hijacking becomes almost impossible via JavaScript(have read some browsers do give out this info to JavaScript but support is increasing). And cookies have a same origin policy. But still using tools like fiddler a determined hacker will be able to access this info.

But the hacker should be able to sniff the network to get to this info.


## No App Cookie Found

- New user
- Existing customer
    - never logged in
    - app cookies were deleted

### existing customer scenarios with no cookie

1. Session starts in Native App and handoff to WkWebView (web page)
1. Session starts in Web App (Shop) and handoff to prospect cart
1. Session starts in Web app (Shop) and handoff to customer cart
1. Session starts in HomePage (anonymous) and login to Account Page

### App Cookie Exists

Existing Customer previously logged in.
