---
title: 'Security POC'
---
# Security POC

## Goal:

Create a small representative application with a DW3 structure and enable _Soda Pop Security_ to test out various scenarios.

### DW3 structure:

1. Nx (_mon-repo structure_)
2. Angular 9 (_ Client framework_ )
3. NodeJS 12+  (_base JS runtime_)
4. HapiJS :Port 3000 (_Web Server_)
5. HapiJS :Port 4200 (_Mock Api Server_)

Web Server will have 2 basic functions:

1. Static Web Server
2. API Orchestration Server

This will allow requests for the client app to be delivered at address
https://localhost:3000/{view route}

And any API calls at

https://localhost:3000/api/{api route}

Any other routes will be a static asset like fonts, images script files.

Because all routes begin with `https://localhost:3000/` the client app can use relative routing, hence a call for an API will be to `/api/*`
The client will not make calls to any external services, all XHR will be to the base `https://localhost:3000`

### API Orchestration

Any XHR requests will go thru the `/api/` hapi plugins.  In our experiment most `api` requests will be authenticated before serving any external data.  This _pop signature auth_ validation is only part of our experiment, the other part is to determine the best manner to generate, store and transmit the various parts of the pop signature, and finally how to extend these patterns to allow a single sign on authenticated status across multiple apps in different sub-domains. (_in our experiment this will be handled with ports_)

### Mock API

The Mock API service is another `NodeJS`/`HapiJS` service that loads a set of static JS files as content to be delivered and exposes a set of functional API to handle the necessary authentication and data requests for our experiment.

# Client - Server : Pop Security

##  Client Initialization

When our client application first startup it checks the browser sessionCookies for a `usn` cookie, if this does not exist the client starts a process to initialize the browser session:

   1. it generates a new crypto-key pair (using native browser crypto.subtle) , stores it as a jwk in IndexedDb (with private key property extractable: false
   2. client sends request to web-server for a session token, sending the public-key in the request
   3. web-server generates a JWT including
       a. the `aud` (client id baked into the web-server that served the client)
       b. the `usn` ( this is just a generated UUID )
       c. the provided cnf (client public key).
   4. The web server signs this with a private key
   5. The web server adds the token to the response as a secure HttpOnly cookie (not visible to the client).
       a. make sure the cookie has an assigned domain so that it can handle sub-domains.
   6. The web server then adds the same `usn` to the response as a separate sessionCookie (visible to the client).
   7. The client receives back a [200] response and validates that a `usn` sessionCookie has been generated.

## Public Data Request

   Public data requests can be made without a logged in state (anonymous). The client page will request public data to display on the first view. This could be anything, we merely need to show pop authentication checks on the web-server.

   The request will include a `nonce` token in the request header signed with the private key previously generated in the IndexedDb.
   The `nonce` token has the following fields:

   1. `ehts`:  an array of header field property names to be included in the fingerprint clob
   2. `edts`:  a 64 character hexadecimal fingerprint derived from a 256Sha hashing of the contentated header properties listed in the `ehts`

   The `ehts` will include:
      - [ 'method', 'uri', 'usn' ] for GET requests
      - [ 'method', 'uri', 'usn', 'body' ] for POST/PUT/DELETE requests

## Request Validation

The server will receive a request with a `nonce` token in the request header and a `session token` (_anonymous jwt_) in the _secure HttpOnly_ cookies.

The Web server does the following:

1. make a request to Mock API for _token signatory public key_.
2. validates the signature of the `session_token`
3. validate the `usn` of the `session_token` matches the `usn` of the _session cookie_
3. read the `cnf` of the `session_token`
4. use the `cnf` to validate the signature of the `nonce`
5. read the `ehts` of the nonce
6. concatenate all the listed fields into a _clob_
7. 256SHA encrypt the ehts _clob_ to create the request fingerprint
8. compare the _request fingerprint_ with the `edts` of the `nonce`

Once a request is validated it can be executed.


# Authentication

The user can login with a password (pick anything for the mock POC)

*user*: _tester_
*pass*: _test12354_

- The client sends credentials over https to the web server.
    _ yes, create local https using self signed certs in local_

- The web-server will validate the request then send to mock api server to ask for an authentication token.
- Once received the web-server will send
   1. the authentication token,
   2. the `usn` (_unique session number_)
   2. the `aud` (_client id_)   _this should be in the web server config_
   3. the `cnf` (_client public-key_)
   to another mock api service asking for an authorized ID Token. This ID Token will include 4 values sent along with the JWT basics:

   1. `iss`   - url of the token issuing authority
   2. `iat`   - time stamp when token was issued at
   3. `exp`   - time stamp when token will expire (interval is configurable)

   the ID Token will also include the `sub` claim - the user id _(for our PC this can be the user name passed in at login)_

   The ID Token is signed by the mock API service private key

- Once the token is received it is passed back in the XHR response.

The client receives the `ID Token` and stores it in the HTML5 browser _sessionStorage_ to be added to every XHR there-after.


# authenticated request

- client makes request to web-server for some private data
- request includes nonce as described above.
- web-server validates request by calling mock-api for public-key to validate signature of ID-Token
- use local private key to validate session token
- verify cnf and usn are same in both session token and ID token and sessionCookie
- use cnf to validate signature of nonce
- use nonce to validate request
- once validated web-server sends request to mock api server to get private data
- once returned the response is forwarded to the client.
- client receives response and displays the private data.
