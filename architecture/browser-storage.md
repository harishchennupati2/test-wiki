---
title: 'browser storage'
---

[Home](../Home.md)


# Browser Storage

The following is a description of the various Browser Storage databases used across various apps.
Browser storage access is controlled by the domain address.  Thus, keys stored from an app at
`https://my.t-mobile.com`
cannot be access from a script delivered from
`https://www.t-mobile.com`


# Shop

`https://tos-shop-dev.dd.stg.kube.t-mobile.com`

## sessionStorage

| key |  value |
| --- |  ----- |
| IpTabId | 6606318924 |
| abtest | true |

## localStorage

| key                               | value                                                                                                                |
|-----------------------------------|----------------------------------------------------------------------------------------------------------------------|
| invoca_id                         | i-d024135f-04df-4d8f-ac2e-f2c3be6d0997                                                                               |
| lpLastVisit-78100234              | 1560379961742                                                                                                        |
| FSR_TMOBILECOM_OBJ                | {"SESSION": {"rpid":"m186bd4404a84a3aa3979bee393f446f","mid":"m1358ec56a914308a07e27b541a86838","rt":false,"rpn":0}} |
| lp_engagementId                   | 1331208714                                                                                                           |
| FSR_TMOBILECOM_BLOB               |                                                                                                                      |
| rxvt                              | 1560287913156                                                                                                        |
| bc_persist_props                  | {"distinct_id":"16b481c24f87eb-02e039a42cdf0a-37647e03-1fa400-16b481c24f9c57","bc_persist_updated":1560282932475}    |
| ruxitagent_82A30F7C9E1E27D8_Store | OK(BF)                                                                                                               |
| dtCookie                          | 5%240Q4QAVA76TQ74M4PLG36INRT683DT1JS%7C82a30f7c9e1e27d8%7C1 |
| name                              | ruxitagent                                                  |
| confi                             | app%3D82a30f7c9e1e27d8%7Ccors%3D1%7CfeatureHash%3DICA2SVfhqru%7Csrsr%3D25000%7Cxb%3D.*demdex%5Ebs.net.*%5Ep.*omtrdc.*%5Ep.*4seeresults%5Ebs.com.*%5Ep.*triggeredmail.*%5Ep.*foresee%5Ebs.com.*%5Ep.*clicktale%5Ebs.net.*%5Ep.*%5Ebs.answerscloud%5Ebs.com.*%5Ep.*%5Ebs.liveperson.net.*%7CreportUrl%3Dhttps%3A%2F%2Fbf06119kov.bf.dynatrace.com%2Fbf%7Crdnt%3D1%7Cuxrgce%3D1%7Csrms%3D1%2C1%2C%2C%2C%7Cuxrgcm%3D100%2C25%2C300%2C3%3B100%2C25%2C300%2C3%7Cmd%3D1%3DcAMCV_1358406C534BC94D0A490D4D%2540AdobeOrg%2C2%3DcClicktaleReplayLink%7ClastModification%3D1560190514336%7CdtVersion%3D10169190521113456%7Ctp%3D500%2C50%2C0%2C1%7Cuxdcw%3D1500%7Cbs%3D1%7Capp%3D82A30F7C9E1E27D8%7CagentUri%3Dhttps%3A%2F%2Fjs-cdn.dynatrace.com%2Fjstag%2F147f273fa2a%2Fruxitagent_ICA2SVfhqru_10169190521113456.js%7Cdomain%3Dt-mobile.com |
| featureHash                       | ICA2SVfhqru           |
| buildNumber                       | 10169190521113456     |
| lastModification                  | 1560190514336         |
| ct.ls.1560282932827.1122111       | {"v":1,"type":1,"item":{"sid":85420779995744,"created":1560282932827,"expires":1560299396522}}                       |
| rxvisitid                         | BWCTLGPGHZSCAUBYUNLAQXTXDMCJUBQD                                                                                     |
|                                   |                                                                                                                      |

### key-store

| key | Value |
| --- | ----- |
| key-pair | {publicKey: {CryptoKey}, privateKey: {CryptoKey}} |

## Cookies

As of 6/12/19 4:30 pm

| key                                       | domain                                 | expire                   | size |
|-------------------------------------------|----------------------------------------|--------------------------|------|
| AA003                                     | .atdmt.com                             | 2019-09-10T23:29:48.093Z | 72   |
| ATN                                       | .atdmt.com                             | 2021-05-22T22:18:44.471Z | 55   |
| fr                                        | .facebook.com                          | 2019-09-10T23:29:47.743Z | 44   |
| pt                                        | .ispot.tv                              | 2021-06-11T23:29:47.181Z | 134  |
| AAMC_tmobile_0                            | .t-mobile.com                          | 2019-12-09T23:29:42.000Z | 24   |
| AMCVS_1358406C534BC94D0A490D4D%40AdobeOrg | .t-mobile.com                          | N/A                      | 42   |
| AMCV_1358406C534BC94D0A490D4D%40AdobeOrg  | .t-mobile.com                          | 2021-06-12T23:29:42.000Z | 302  |
| LPSID-78100234                            | .t-mobile.com                          | N/A                      | 36   |
| LPVID                                     | .t-mobile.com                          | 2021-06-11T23:29:52.000Z | 27   |
| \_fbp                                     | .t-mobile.com                          | 2019-09-10T23:29:47.000Z | 33   |
| \_gcl_au                                  | .t-mobile.com                          | 2019-09-10T23:29:42.000Z | 31   |
| check                                     | .t-mobile.com                          | N/A                      | 9    |
| gpv_v10                                   | .t-mobile.com                          | 2019-06-12T23:59:42.000Z | 25   |
| incap_ses_235_850966                      | .t-mobile.com                          | N/A                      | 76   |
| invoca_session                            | .t-mobile.com                          | 2119-05-19T23:35:48.000Z | 1840 |
| mbox                                      | .t-mobile.com                          | 2021-06-13T23:29:43.000Z | 108  |
| mboxEdgeCluster                           | .t-mobile.com                          | 2019-06-13T00:00:42.000Z | 17   |
| nlbi_850966                               | .t-mobile.com                          | N/A                      | 59   |
| s_cc                                      | .t-mobile.com                          | N/A                      | 8    |
| s_ecid                                    | .t-mobile.com                          | 2021-06-11T23:29:42.124Z | 52   |
| sessionFldEng15sec                        | .t-mobile.com                          | 2019-06-12T23:59:57.000Z | 19   |
| sessionFldEng5sec                         | .t-mobile.com                          | 2019-06-12T23:59:47.000Z | 18   |
| sessionFldLoad                            | .t-mobile.com                          | 2019-06-12T23:59:42.000Z | 15   |
| utag_main                                 | .t-mobile.com                          | 2020-06-11T23:29:42.000Z | 175  |
| aam_uuid                                  | .tos-shop-dev.dd-stg.kube.t-mobile.com | 2019-07-12T23:29:42.000Z | 46   |
| LPSessionID                               | va.v.liveperson.net                    | N/A                      | 33   |
| LPVisitorID                               | va.v.liveperson.net                    | 2021-06-11T23:29:44.415Z | 33   |
|                                           |                                        |                          |      |
