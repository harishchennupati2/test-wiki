---
title: 'environments'
---

[Home](../Home.md)

# Web Service environments

All TOS applications have a web service hosted in Kubernetes.  These Kubernetes environments have a naming pattern:

`tos-{*app-name*}-{*env*}.{*cluster*}.kube.t-mobile.com`

| environment | cluster  |
| ----------- | -------- |
| Non-Prod    | `dd-stg` |
| Prod        | `dd-prd` |


| App Key | App Name | Description |
| --- | ---- | ----|
| shp | Shop | Unified Shopping Experience: Browse, Detail, Cart, Checkout |
| pub | Pub  | Payments, Usage, Billing |
| dam | Dam  | Digial Account Management |
| flx | Flex | Global components |


|:idx:| env key | Description |
| --- | ------- | -----------|
|: 0 :| bld | Build: Jenkins Build Agents - This environment is shared across all TOS apps |
|: 1 :| reg | Regression: functional testing of live deployed application|
|: 2 :| plb | PLab: Performance Testing |
|: 3 :| dev | Deployed app for developer testing: deployments throttled to no more than once/30 minutes |
|: 4 :| qat | On demand deployment for QAT/UAT testing|
|: 5 :| stg | Staging: Prod like environment for final verification or demonstration |
|: 6 :| prd | Live Production site - comes with Blue/Green/Canary flavors |
|: 7 :| pvw | Prod Preview - for web authors to view changes to assets a _private_ application prior to publishing to live production |


## Shop

| Environment | Webserver | AEM Author | Notes |
| --- | --- | --- | --- |
| REG | https://tos-shop-reg.dd-stg.kube.t-mobile.com | https://tos-aem-reg.corporate.t-mobile.com:4502|
| PLB | https://tos-shop-plb.dd-stg.kube.t-mobile.com | N/A |
| DEV | https://tos-shop-dev.dd-stg.kube.t-mobile.com | https://tos-aem-dev.corporate.t-mobile.com:4502 |
| QAT | https://tos-shop-qat.dd-stg.kube.t-mobile.com | https://tos-aem-qat.corporate.t-mobile.com:4502 |
| QA1 | https://tos-shop-qa1.dd-stg.kube.t-mobile.com | https://tos-aem-qat.corporate.t-mobile.com:4502 | V3 Environment
| STG | https://tos-shop-stg.dd-stg.kube.t-mobile.com | https://tos-aem-stg.corporate.t-mobile.com:4502 |
| PPV | https://tos-shop-ppv.dd-prd.kube.t-mobile.com | Premium Upgrade |
| PRD ONLINE | https://tos-shop-prd.dd-prd.kube.t-mobile.com | Premium Upgrade |
| PRD OFFLINE | https://tos-shop-prd-offline.dd-prd.kube.t-mobile.com | Premium Upgrade |
