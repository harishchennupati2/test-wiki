---
title: 'Digital Web Architecture'
---
[Home](../Home.md)


# DW3 Design Criteria

DW3 was designed to improve performance on 3 pillars of application success:

1) Customer Experience
> By improving the architecture to optimize the speed of content delivery to the user we can improve the customer experience with any application developed using DW3.

2) Developer
> By optimizing the tools, workspace and design patterns used in developing applications in DW3 we can improve the developer experience, enabling them to work more efficiently and with greater job satisfaction.  This optimization has a direct correlation to "Time to Value" in it's support of delivering features with higher quality in less time.

3) Operations
> By optimizing the runtime deployment architecture and tools we can minimize the amount of manual labor required to release, deploy and support applications developed using DW3.  This not only has a direct correlation to "Time to Value" but also significantly reduces the cost to support applications during and after release.

# DW3 Features

- Headless Content Management
- Scalable Content Delivery
- Mono-Repo
- Trunk Based Development
- Containerized Web Service( Kubernetes : Conduktor )
- L2 Cache: Redis
- Infrastructure As Code
- Configuration As Code
- CICD
- Angular +
- Angular-Material
- NgRx + Entities + Data
- Unit Tests: Jest + Karma / Mocha + Chai + sinon
- UI Fn Tests: Webdriver.io + Mocha + Chai +sinon
- Sauce Labs
- CxLabs Device Matrix
- Performance Tests: Locust
- Security: Ternary Pop-Jwt
- Workspace Automation:
  - Nx Affected
  - git hooks