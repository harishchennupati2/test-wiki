---
title: 'Monorepo library organization'
---
[Home](../Home.md)

# Monorepo Library Organization

The consistency and organization of libs is important for maintaining a clean monorepo. It is also important that the Nx workspace is organized in such a way that unnecessary dependencies are not created, which will improve the CI process as well as the accuracy of the `dep-graph`.

The organization of this monorepo is based on Nrwl's book "Enterprise Angular Monorepo Patterns". The main takeaway of this book is the idea that files should *not* be organized by file-type, but instead by feature and related functionality.

The book suggests organizing libs by one of their 4 grouping types, referred to as **scope** as denoted below:

## Component Scope

### 1. Feature

- Usually app-specific
- Contains "routable" components (ex: component that displays when routing to /cell-phones)
- Depict a business use case
- Note: a folder containing multiple features with a similar use case are referred to as *feature-shells*.

>**Example**:

> Product List and Product Detail pages are considered separate features under `libs/shop`.


### 2. UI

- Presentational (dumb) components
- Feature libs use UI libs to build the view of a page

>**Example**:

> The product-list feature contains a UI lib called `product-grid`, which takes a list of products as an @Input() and renders a PLP grid.



### 3. Data-access

- Files and services for interacting with backend systems
- NgRx functionality typically lives here

>**Example**:

> the slice of NgRx state pertaining to stores and locations lives under `libs/shared/data-access`.


### 4. Utility

- Shared internal services & utilities

>**Example**:

> a simple in-house JSON parser that can be used in many places lives under `libs/utils`.


## Project Names

  Project names refer to the hyphenated name of a lib in the **`nx.json`** and **`angular.json`**. For consistency, these project names typically match the directory in which the module lives. The modules for these libs will also match the directory structure as closely as they can.

  For example, the module for the **_UNAV UI_** lib is called `SharedUiUnavModule` and lives under `libs/shared/ui/unav`.
The project name in the **`nx.json`** is `shared-ui-unav`.
The TypeScript alias in the **`tsconfig.json`** is `@tmo/shared/ui/unav`.


## Examples of Lib Organization

As mentioned previously, we do ***not*** want to organize file by their type. For example, consider the following directory structure:

** DO NOT DO THIS **
```
libs
  |- shop
      |- components <-- don't do this!
      |- directives
      |- pipes
```

This folder structure is considered an ***anti-pattern*** in an Nx workspace as it creates unnecessary dependencies and makes it difficult for a developer to figure out which pieces of functionality are related to each other.

Instead, an example of a `libs` directory for the _product list_ page may look something like this:

```
libs
  |- shop
      |- feature
          |- product-list.module.ts
          |- product-list.component.ts <-- this is the "routable" component
      |- ui
          |- product-grid
          |- product-card
          |- product-filters
      |- data-access
          |- +state

  |- shared
      |- ui
          |- unav
      |- util
          |- json-parser
      |- data-access
          |- ...

```

As you can see, this structure groups folders by their **domain** instead of **file type**. Notice how the `libs/shop/**` libs contain `ui` and `data-access` libs that are specific to the shop application, but `libs/shared/**` libs can also contain `ui` and `data-access` libs that can be used across multiple applications.

## Types vs Scopes

In the above example it may seem that the `libs/shared` folder and the `libs/shop` folder are both organizing components by **type**. but actually it is organizing these library components by **scope**.

### What is the difference between a **Type** and a **Scope** ?

A component Type is a subset of Scope.

Type is a categorization of structure:

- directive
- pipe
- service
- ...

Scope is a categorization of purpose

| Scope       | Purpose                            |
| ----------- | ---------------------------------- |
| ui          | _user interface element (widget)_ |
| util        | _general purpose library of functions_ |
| data-access | _collection of functions specific to CRUD operations with local data store (NgRx)_ |
| feature     | _routable view_ |
