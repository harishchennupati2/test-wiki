---
title: 'Readme'
---
# Digital Web Architecture

Discussion about Architecture, patterns, practices, standards

## DWA 1.0

- CQ
- JQUery
- Handlebars
- Dot Net
- Jira

## DWA 2.0

- AEM 6.1
- AngularJS
- Bitbucket Data Center

## DWA 3.0

- AEM 6.5
- Angular 8
- NodeJS
- HapiJS
- POP-JWT
- Bazel
- Proto
- Docker
- Kubernetes
- Bitbucket.org
- Rally

## DWA 3.1

- Angular 9
- NestJS
- Soda POP Web Security
- GitLab
- Jira

## DWA 4.0

- Serverless: Lambda
- Service Mesh
- ESB (service orchestration api)

