#### TOS Build Status:

[![Quality Gate Status](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/api/project_badges/measure?project=tmo-one-site&metric=alert_status)](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/dashboard?id=tmo-one-site)[ ![Coverage](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/api/project_badges/measure?project=tmo-one-site&metric=coverage)](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/dashboard?id=tmo-one-site)[ ![Code Smells](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/api/project_badges/measure?project=tmo-one-site&metric=code_smells)](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/dashboard?id=tmo-one-site)[ ![Bugs](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/api/project_badges/measure?project=tmo-one-site&metric=bugs)](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/dashboard?id=tmo-one-site)[ ![Duplicated Lines (%)](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/api/project_badges/measure?project=tmo-one-site&metric=duplicated_lines_density)](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/dashboard?id=tmo-one-site)[ ![Security Rating](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/api/project_badges/measure?project=tmo-one-site&metric=security_rating)](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/dashboard?id=tmo-one-site)[ ![Reliability Rating](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/api/project_badges/measure?project=tmo-one-site&metric=reliability_rating)](https://tos-sonarqube-dev.dd-stg.kube.t-mobile.com/dashboard?id=tmo-one-site)

the above are the build images are pulled directly from our build server. If you are not on the T-mobile domain, or on a vpn, you may not see images above.

# TOS Wiki

![](logos/onesite-magenta.png)

## What is TOS ?

T-mobile One Site. This moniker was coined when T-mobile (NextGen) and my.T-mobile (eServices) teams joined under a single director.
The goal was to operate the customer facing web applications as one site, one team.

TOS is based on the _T-Mobile Digital_ **Web 3.0** architecture.
There are a lot of new patterns, practices, tools and libraries packed into **Web 3.0**.

Checkout the [March of Icons](#March-of-the-icons) below to get an idea of the many patterns in play.

or go to [Web 3.0 Architecture](#Architecture) to learn more about the architecture, what and how it evolved.

## Developer Setup

- [Quick Start Guide](Quick%20Start%20Guide.md)
- [Developer Guide](Developer%20Guide.md)
- [Contributing Guide](Contributing%20Guide.md)
- [Advanced Local Development](Local%20Dev.md)
- [AEM Quick Start Guide](AEM%20Quick%20Start%20Guide.md)
- [Unit Test Guide](UNIT%20TEST%20GUIDE.md)

## Configuration

- [`package.json` dependencies](Configuration/package_json_deps.md)
- [`package.json` devDependencies](Configuration/package_json_dev_deps.md)

## The Workspace

- [git submodules](git-tricks/submodules.md)
- [Nx Tags](Nx%20Tags.md)
- [Generating new universal apps](Generating%20new%20universal%20app)
- [Generating angular services / hapi plugins from proto files](Generating%20angular%20services%20and%20hapi%20plugins%20from%20proto%20files.md)
- [Monorepo Library Organization](architecture/monorepo-library-organization.md)

## API

- [Authentication and CRP for local development](Authentication%20and%20CRP%20for%20local%20development.md)
- [Generating new `/api/*` endpoints](<Generating%20new%20API%20(Hapi%20plugin%20and%20Angular%20service).md>)
- [Postman Collections](postman/Postman.md)
- [Mock Data Files](mock-data/readme.md)

## Release Management

- [Continuous Integration](release-management/continuous-integration.md)
- [Continuous Deployment](release-management/continuous-deployment.md)

## Architecture

[architecture](architecture/readme.md)

- [Client/Server application configuration](architecture/configuration.md)
- [Deployment Architecture](architecture/Deployment%20Architecture/Architecture.md)
- [Digital Web 3.0 Architecture](architecture/Digital%20Web%203.0%20Architecture.md)
- [S3 Asset Store](architecture/s3-asset-store.md)
- [Cross Browser Compatibility](architecture/browser-compatibility-guide.md)

## Security

- [Web Security Notes](architecture/web-security/README.md)
- [scenarios](architecture/web-security/scenarios.md)
- [token samples](architecture/web-security/tokens.md)
- [Pop Jwt Notes](architecture/web-security/POP_JWT.pdf)

### UML diagrams

- [SEQ: native app handoff](/architecture/web-security/app.handoff.puml)
  - [image](architecture/web-security/app_handoff.png)
- [SEQ: anonymous handoff](architecture/web-security/anonymous-handoff.jtw.puml)
  - [image](architecture/web-security/anonymous-handoff_jtw.png)
- [SEQ: anonymous handoff from deep link](/architecture/web-security/deep-link.anonymous.jwt.puml)
  - [image](architecture/web-security/deep-link_anonymous_jwt.png)
- [ACT: pop-signature validation](architecture/web-security/validate.pop-signature.puml)
  - [image](architecture/web-security/validate_pop-signature.png)

## Clearwater

- [Sequence Diagrams](clearwater/sequence_diagrams/clearwater_sequence_diagrams.md)

## Styles

- [TMO Styles Update Guide](TMO%20styles%20-%20Update%20Guide.md)

## Analytics

- [window.digitalData Initialization & Events Creation](Analytics%20Initialization%20&%20Events%20Creation.md)

## Others Articles

- [Wiki Notes](wiki.md)
- [Rally Integration](rally-integration.md)
- [Environments](architecture/environments.md)

---

# March of the icons

<a name="March-of-the-icons"></a>

The following languages, frameworks, libraries, tools and infrastructure are used in the development, testing, release and monitoring of applications within the TOS Family of applications.

#### Source Code Languages

<div style="padding-left:50px">

<img src="logos/html5.png" title="HTML5" height="50" />
<img src="logos/scss.png" title="SCSS / SASS / CSS3" height="50" />

<img src="logos/typescript.png" title="TypeScript" height="50" />
<img src="logos/java.png" title="java" height="50" />
<img src="logos/json.png" title="json" height="50" />
<img src="logos/yaml.png" title="yaml" height="50" />
<img src="logos/swagger.png" title="Swagger" height="50" />

</div>

#### Libraries & Framworks

<div style="padding-left:50px">

<img src="logos/angular.png" title="Angular: Client SPA Framework" height="50" />
<img src="logos/angular-material.png" title="Angular-Material: Presentation Design Framework" height="50" />
<img src="logos/redux.png" title="Redux: Application State Pattern" height="50" />
<img src="logos/rxjs.jpeg" title="RxJs: Reactve Extensions for Javascript -- Observables" height="50" />
<img src="logos/NgRx.png" title="NgRx: Redux + RxJS + Angular" height="50" />

<img src="logos/hapijs.png" title="HapiJS: Node Web Server Framework" height="50" />
<img src="logos/nestjs.png" title="NestJS: Node Web Server Framework" height="50" />

<img src="logos/nodejs.png" title="NodeJS: Javascript Engine Service" height="50" />

<img src="logos/aem.png" title="AEM: Adobe Experience Manager (CMS)" height="50" />

</div>

#### Automation: CICD

<div style="padding-left:50px">

<img src="logos/bazel.png" title="Bazel: Build framework" height="50" />
<img src="logos/jenkins.png" title="Jenkins: Build Scheduler" height="50" />

</div>

##### Testing Frameworks and tools

<div style="padding-left:50px">

<img src="logos/jest.png" title="Jest: Headless JS Test runner"  height="50" />
<img src="logos/karmajs.png" title="KarmaJS: In Browser Test Runner" height="50" />

<img src="logos/mochajs.png" title="MochaJS: Test framework" height="50" />
<img src="logos/chaijs.png" title="Chaijs: Assertion Library" height="50" />
<img src="logos/sinonjs.png" title="SinonJS: Mocking library" height="50" />

<img src="logos/webdriver.png" title="WebDriver IO: Acceptance Test Driver" height="50" />
<img src="logos/user1st.jpeg" title="User1st: Accessibility Acceptance Testing" height="50" />
<img src="logos/applitools.jpeg" title="AppliTools: Visual Difference Acceptance Testing" height="50" />

<p>
<img src="logos/saucelabs.png" title="Sauce Labs: Acceptance Test Service" height="50" />
<img src="logos/device_matrix.png" title="Device Matrix: T-Mobile Device Array for Automated Acceptance Testing" height="50" />
</p>

<img src="logos/sonarqube.png" title="SonarQube: Quality Metrics Dashboard" height="50" />

<img src="logos/locust.io.jpeg" title="Locust: Performance Test Runner" height="50" />
<img src="logos/artillery.io.png" title="Artillery: Performance Test Tool" height="50" />

</div>

##### Telemetry Metrics Dashboards

<div style="padding-left:50px">

<img src="logos/splunk.png" title="Splunk: Log miner" height="50" />
<img src="logos/grafana.jpeg" title="Grafana" height="50" />

</div>

#### Infrastructure: Deployment

<div style="padding-left:50px">

<img src="logos/gitlab.png" title="GitLab: Source Repository & Devops Automation" height="50" />

<img src="logos/docker.png" title="Docker" height="50" />
<img src="logos/kubernetes.png" title="Kubernetes: Container Manager" height="50"  />

<img src="logos/aws.png" title="AWS" height="50" />
<img src="logos/s3-bucket.png" title="S2: Simple Storage Solution" height="50" />

</div>

#### Configuration As Code

<div style="padding-left:50px">

<img src="logos/percy.logo.png" title="Percy: Configuration As Kode Editor" height="50" />

</div>
