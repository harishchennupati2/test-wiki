This is not a style guide, just a summary of the class and utility changes between the original styles (those in `/shared/libs/styles`) and the new version (those in `/shared/libs/tmo-styles`). We are currently working on putting together a style guide which will have much more comprehensive documentation of the usage of all these utilities, styles, and components.

## Adding TMO styles to a project

The styles must be added at the application level. Import global styles into your app’s `styles.scss` file.

```
@import '~normalize.css/normalize.css';
@import '../../../../libs/shared/tmo-styles/src/lib/fonts/tele-grotesk-next';

@import '../../../../libs/shared/tmo-styles/src/lib/variables';
@import
'../../../../libs/shared/tmo-styles/src/lib/material-theme/theme-core';
@import '../../../../libs/shared/tmo-styles/src/lib/material-theme/theme-default';
@import '../../../../libs/shared/tmo-styles/src/lib/tmo-theme';
```

To access tmo-styles utilities in your component-level styles, your project’s configuration in angular.json must include this line in `[project config].architect.build.options`:

`"stylePreprocessorOptions": { "includePaths": ["libs/shared/tmo-styles/src/lib"] }`

Then in any component-level styles that need access to them, import variables:
`@import 'tmo-theme/variables.css';`

This will give you access to many of the sass mixins and variables that you see defined here.

## Typography

### Base font size

The base font size is 16px. It hasn’t changed, but is now being used more heavily, so things generally appear larger and more spaced out.

### Font families

There are now 2 font families available:

#### Heading font: **Tele-Grotesk-Next**

Generally this is only used for headings, but there may be some exceptions to this.

`$heading-font: Tele-Grotesk-Next, Arial, Helvetica, 'Helvetica Neue', sans-serif;`

Example usage:

```
.special {
  font-family: $heading-font;
  ...
}
```

#### Body font: **System font**

`$body-font: BlinkMacSystemFont, -apple-system, Segoe UI, Roboto, Helvetica, Arial, sans-serif;`

This font family is used for everything else. Unless you are using one of the display headers, this font family will be used by default.

#### Body font variations

| Description | Previous        | Current             |
| ----------- | --------------- | ------------------- |
| Body text   | `.body`         | _Not needed_        |
| Bold text   | `.body--bold`   | `.font-weight-bold` |
| Small text  | `.body--small`  | `.text-small`       |
| Legal text  | `.legal`        | `.text-legal`       |
| Medium size | `.body--medium` | `Not needed`        |

#### Links

The link style needs the `.link` class applied. Instead of the usual text-decoration property, we’re using `border-bottom` to style it, which means that you can’t apply this link class to a block element (try nesting it instead).

## Breakpoints

The breakpoints are different from the last version.

#### Previous

```
$xl-desktop-size: '(min-width: 1280px)';
$desktop-size: '(min-width: 1024px)';
$tablet-size: '(min-width: 641px) and (max-width: 1023px)';
$mobile-size: '(max-width: 640px)';

$tablet-max: 1008px;
$mobile-max: 640px;
$mobile-min: 320px;
```

#### Current

```
$desktop-xl-min: 1680px;
$desktop-min: 1280px;
$desktop-sm-min: 1024px;
$tablet-min: 768px;
$mobile-min: 320px;
```

### Breakpoint utilities

All of these mixins are inclusive of the media size they reference.

**Getting smaller**

```
@include for-desktop-down {}
@include for-desktop-sm-down {}
@include for-tablet-down {}
@include for-phone-only {}
```

**Getting larger**

```
@include for-tablet-up {}
@include for-desktop-sm-up {}
@include for-desktop-up {}
@include for-desktop-xl-up {}
```

#### Replacement examples

| Previous                                                     | Current                       |
| ------------------------------------------------------------ | ----------------------------- |
| `@media (max-width: \$tablet-max) {}`                        | `@include for-tablet-down {}` |
| `@media (max-width: \$mobile-max) {}`                        | `@include for-phone-only {}`  |
| `@include break-max(\$mobile-max) {}`                        | `@include for-phone-only {}`  |
| `@media #{\$mobile-size} {}`                                 | `@include for-phone-only {}`  |
| `@include break-min(\$mobile-max) {}`                        | `@include for-tablet-up {}`   |
| `@include break-min-max() | _Still included, do we need it?_ |

## Spacing

Spacing has not be replaced 1:1 in the new styles. In the original styles, we applied spacing using spacer elements between content. In the new styles, we are using a combination of utility classes and spacing variables (referenced in component-level styles).

## Utilities

| Name | Width   | Sass variable   |
| ---- | ------- | --------------- |
| 3xs  | `4px`   | `$spacing-3xs;` |
| xxs  | `8px`   | `$spacing-xxs;` |
| xs   | `16px`  | `$spacing-xs;`  |
| sm   | `24px`  | `$spacing-sm;`  |
| med  | `40px`  | `$spacing-med;` |
| lg   | `56px`  | `$spacing-lg;`  |
| xl   | `80px`  | `$spacing-xl;`  |
| xxl  | `120px` | `$spacing-xxl;` |

## Classes

These utilities are set to `!important`. This is a way that we can partially employ some of the benefits of utility-first approach to our styles, but retain the consistency of scoped styles. See: https://csswizardry.com/2016/05/the-importance-of-important/
If the class is no longer desired, simply remove it.

#### Reset padding

#### Previous

`.no-pad`

#### Current

```
.padding-0
.padding-top-0
.padding-bottom-0
.padding-left-0
.padding-right-0
.padding-horizontal-0
.padding-vertical-0
```

### Reset margin

#### Previous

`.no-marg`

#### Current

```
.margin-0
.margin-top-0
.margin-bottom-0
.margin-left-0
.margin-right-0
.margin-horizontal-0
.margin-vertical-0
```

#### Using spacers

All the sizes listed above have corresponding spacing classes. Here's some examples:

```
.margin-top-xs
.margin-horizontal-med
.padding-vertical-med
```

## Buttons

The new main button style has an animated caret in it, and uses Angular Material buttons. Each of these classes must be used with mat-button, mat-flat-button, or mat-stroked-button, depending on the style you want to achieve.

| Description        | Previous                               | Current                        |
| ------------------ | -------------------------------------- | ------------------------------ |
| Standard button    | `.button--standard`                    | `.cta-button`                  |
| Full-width button  | `.button--standard.button--full-width` | `.cta-button.full-width`       |
| Fixed-width button | _Not needed previously_                | `.cta-button.fixed-width`      |
| Inline button      |                                        | `.cta-button.inline-button`    |
| Small button       | `.button--standard.button--small`      | `.cta-button.cta-button-small` |
| Large button       |                                        | `.cta-button.cta-button-large` |

## Angular Material

The “primary” and “accent” colors in our Angular Material theme have been switched, so all references will need to be reversed.
Theme is now separated into tmo-theme and tmo-dark-theme
To use the dark theme, just wrap all your content in `.tmo-dark-theme`

## Colors

Most of the color variable names are the same, with these exceptions.

| Previous              | Current                     | Explanation    |
| --------------------- | --------------------------- | -------------- |
| `$tmo-magenta`        | `$tmo-magenta-brand`        |                |
| `$tmo-gray5`          | `$tmo-gray05`               |                |
|                       | `$tmo-yellow-brand`         |                |
| `$disabled`           | `$disabled-text-color`      | More specific  |
| `$disabled-dark`      | _No replacement_            | Not being used |
| `$default-text-color` | `$default-light-text-color` |                |