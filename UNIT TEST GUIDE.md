---
title: "unit test guide"
---

# Unit Test Guide #
This document provides the standard guides for unit tests.  Every project in tmo-one-site should follow this guide.  If you have questions or recommendation about unit tests, please slack at dev-tmo-one-site.

## Test Tools ##
While there are many test libraries available, we have picked Jest as test runner, Sinon for mocking, and Chai for assertion.  With that said...

### Test runner (Jest) ###
When you write the test block and execute code before or after test, you use the following functions from the Jest library:

```
describe('libs/shop/product-list/products-data-access.service': () => {

  beforeAll(() => {
    ...
  });

  beforeEach(() => {
    ...
  });

  afterAll(() => {
    ...
  });

  afterEach(() => {
    ...
  });

  it('should behave...', () => {
  });

});

```
Notices that the describe block should contains the file path for better test output.

### Mocking (Sinon) ###
To mock using Sinon, here is an example:

```typescript
const getProducts: SinonStub = sinon.stub();

getProducts.returns([{id: 1}, {id: 2}]);
// or Observable
getProducts.returns(of([{id: 1}, {id: 2}]));
// or Promise
getProducts.returns(Promise.resolve([{id: 1}, {id: 2}]));
```

### Assertion (Chai) ###
For assertion using Chai, here is an example:
```
import { expect } from 'chai';
...
expect({a: 'a', b: 'b'}).deep.equal({a: 'a', b: 'b'});
expect(1 === 1).true;
expect(stub).called;
```
Notices that  you must import `expect` from `chai` to avoid calling assertion functions from wrong libraries.

## More Examples ##
In the following examples, we are testing `ProductsDataAccessService`.  When `getCachedProducts` function is called, it will call `getProducts` in `ProductsService`, cache the value, and forward the return value out.  Here is the coding examples:

### Basic (Not Promise or Observable) ###
Assuming `getCachedProducts` function returns values without Promise or Observable:
```
import { expect } from 'chai';
...

describe('libs/shop/product-list/products-data-access.service': () => {

  const getProductsStub: SinonStub = sinon.stub();
  getProductsStub.returns([{id: 1}, {id: 2}]);
  let productsDataAccess: ProductsDataAccessService;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
      {
        provide: ProductService,
        useFactory: () => {
          getProducts: getProductsStub
        }
      }
    }).compileComponents();

    productsDataAccess = TestBed.get(ProductsDataAccessService);
  });

  afterEach(() => {
    sinon.restore();
  });

  it('can retrieve products', () => {
    let products = productsDataAccess.getCachedProducts();

    expect(getProductsStub).calledOnce;
    expect(products).deep.equal([{id: 1}, {id: 2}]);

    getProductsStub.resetHistory();

    // second call should use the cached value
    products = productsDataAccess.getCachedProducts();

    expect(getProductsStub).not.called;
    expect(products).deep.equal([{id: 1}, {id: 2}]);
  });

});
```

### Observable ###
Assuming `getCachedProducts` function returns values in Observable:
```
import { expect } from 'chai';
import { TestScheduler } from 'rxjs/testing';
...

describe('libs/shop/product-list/products-data-access.service': () => {

  const getProductsStub: SinonStub = sinon.stub();
  getProductsStub.returns(of([{id: 1}, {id: 2}]));
  let productsDataAccess: ProductsDataAccessService;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
      {
        provide: ProductService,
        useFactory: () => {
          getProducts: getProductsStub
        }
      }
    }).compileComponents();

    productsDataAccess = TestBed.get(ProductsDataAccessService);
  });

  afterEach(() => {
    sinon.restore();
  });

  it('can retrieve products', () => {
    const scheduler = new TestScheduler((actual, expected) => {
      expect(actual).deep.equal(expected)
    });
    let products = productsDataAccess.getCachedProducts();

    scheduler.run(helpers => {
        helpers.expectObservable(products)
              .toBe('a', {a: [{id: 1}, {id: 2}] });
    });
    expect(getProductsStub).calledOnce;
    expect(products).deep.equal([{id: 1}, {id: 2}]);

    getProductsStub.resetHistory();
    products = productsDataAccess.getCachedProducts();

    scheduler.run(helpers => {
        helpers.expectObservable(products)
              .toBe('a', {a: [{id: 1}, {id: 2}] });
    });

    // second call should use the cached value
    expect(getProductsStub).not.called;
    expect(products).deep.equal([{id: 1}, {id: 2}]);
  });

});
```
For more information about TestScheduler, visit https://rxjs-dev.firebaseapp.com/guide/testing/marble-testing

### Promise ###
Assuming `getCachedProducts` function returns values in Promise:
```
import { expect } from 'chai';
...

describe('libs/shop/product-list/products-data-access.service': () => {

  const getProductsStub: SinonStub = sinon.stub();
  getProductsStub.returns([{id: 1}, {id: 2}]);
  let productsDataAccess: ProductsDataAccessService;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
      {
        provide: ProductService,
        useFactory: () => {
          getProducts: getProductsStub
        }
      }
    }).compileComponents();

    productsDataAccess = TestBed.get(ProductsDataAccessService);
  });

  afterEach(() => {
    sinon.restore();
  });

  it('can retrieve products', async() => {
    let products = await readFirst(productsDataAccess.getCachedProducts());

    expect(getProductsStub).calledOnce;
    expect(products).deep.equal([{id: 1}, {id: 2}]);

    getProductsStub.resetHistory();

    // second call should use the cached value
    products = await readFirst(productsDataAccess.getCachedProducts());

    expect(getProductsStub).not.called;
    expect(products).deep.equal([{id: 1}, {id: 2}]);
  });

});
```