---
title: 'Postman'
---

[Home](../Home.md)

# Postman

The Core Postman Collection - Updated 10/07/2019

- [Collection](OneSite.postman_collection.json)

Pre-Configured Environments

- [Local](environments/Local.postman_environment.json)
- [Dev](environments/Dev.postman_environment.json)
- [Prod](environments/Local.postman_environment.json)

#Quick Setup

- Import the Collection and the Environments into your Postman.
- Create a valid key pair for `public_key` and `private_key` global variables.
- Execute the RUN ONCE request at the root of this collection

# Key Notes

1. Application Calls that require valid POP tokens will not work until
   1. You have executed the RUN ONCE call at the root of this collection.
   2. You have include a valid key pair for the `public_key` and `private_key` global variables
      ![Example Keys](/images/Example%20Keys.png)
      Notice the data formats are different due to the way the keys are being used.
      Feel free to just use [these keys](/assets/KeyPair.txt)
   3. You have obtained and stored a valid JWT using your `public_key` (see Step 1 in the Pop Creation Script section below)
2. This collection uses a mixture of Environment and Global variables. Environment variables are only used for the hostname within requests. All other variables are globals, only two of which need to be manually added (public and private key as already mentioned)
3. There will be a day when the PoP Creation Script breaks and needs to be updated. If PoP Validation changes in DW3, this script will need to be modified.

# A Bit Deeper

##Run Once Script
This script does two primary things

1.  'sideloads' an external library [RSA-Sign JavaScript]('https://kjur.github.io/jsrsasign/jsrsasign-latest-all-min.js').
    This library is then stored as a global variable for use by the `pop_creation_script`

2.  Generates the `pop_creation_script` and stores that as a global variable.

##The PoP Creation Script
After the Run Once Script has generated the `pop_creation_script`, it can be used for all application calls that require POP authentication.

- It relies on the existence of a valid `jwt` global variable as well as a valid `private_key` global variable for signing in step 2 below.
- It creates a `pop` global variable

1. The Global 'jwt' Variable should be populated with a valid JWT that was obtained by sending the 'public_key' to a JWT Issuing Auth Endpoint
   ![Example JWT Request](/images/Example%20JWT%20Request.png)
   ![Example JWT Test](/images/Example%20JWT%20Test.png)
2. The actual request you are making that requires a POP needs to use the pop_creation_script in the Pre-request Script field by eval'ing it.
   ![Example Pre-Request Script](/images/Example%20Pre-Request%20Script.png)
3. Setting the `Authorization` header of your call to then use the `pop` global variable that is created when that pre-request script is executed
   ![Example PoP Header](/images/Example%20PoP%20Header.png)
