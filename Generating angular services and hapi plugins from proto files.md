[Home](../Home.md)

# Auto Generating Angular Service and Hapi Plugins

TOS uses a set of schema files, proto files and the Bazel compiler to auto generate web api plugins and the angular services that connect to them.  Once generated these server plugins and client services can be imported into your application.

# TLDR #
* We generate one angular lib and one hapi lib for every hapi plugin we wish to create
* These plugins are defined using protocol buffers serving as their schema
* The protocol buffers are located under `libs/shop/api/src/lib/proto`
* The generated code is located under `libs/shop/api/src/lib/generated`
    * The angular interfaces are under `name_api_angular`
    * The hapi interfaces are under `name_api_server`

## Defining a new plugin ##
* You can update the generated code in one of two ways based on your system.
* If your system has bazel installed, the fastest way to update the generated code is to use bazel directly via: `nps swaggerGen.shop.with.bazel`
* Or, if you do not want to install bazel, you can use docker to update the generated code (albeit a bit more slowly): `nps swaggerGen.shop.with.docker`

## Defining a new plugin ##
* To define a new plugin create a new proto file to start
* Then edit `libs/shared/api/src/lib/proto/BUILD.bazel` and define a new build label for your plugin.

If your plugin was named `foo` you would add the following to the build file
```
proto_swagger_gen(
    name = "foo_service",
    proto = "foo_service.proto",
)
```

Then just update the generated code as the last section covered.

## Background ##
A schema is a representation of the structure of the shape of serializable data using a formal language; it describes an interface between what a client can request from a source and what that source will respond with. The idea is that you, as a developer, should define how you want your data to be structured once and only once. That definition should be treated as the sole source of truth for what the shape of the data must be, even across different systems and development languages. We define these interfaces by writing proto files. More information about the syntax for proto files can be found here: https://developers.google.com/protocol-buffers/docs/reference/proto3-spec


Using Bazel — a build system capable of generating code as a pre-compilation step—we have created a build system which enforces that our source code conforms to a schema every time we try to run it.
It does so via code gen which occurs before our application is compiled. If the code does not match the schema, there will be a compilation error.