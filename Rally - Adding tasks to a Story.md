[Home](Home.md)

# How to add sub-task to a story

1. Open the user story in Rally
2. Go to the "Tasks" tab (second tab)
3. Write the name of the task in the text box with label "New Task"
4.1 Then press "Add" button to create the placeholder task
4.2 Then press "Add with details" button to create the task with all the details
5. You will see the task created in the tab "tasks" for that user story.